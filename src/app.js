import express from 'express';
import path from 'path';
import morgan from 'morgan';
import socketIO from 'socket.io';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import logger from './util/logger';
import connectio from './db/connection';

import router from './routes/index';

const app = express();
const http = require('http').Server(app);
const io = socketIO(http);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', router(io));

app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.render('error');
});


http.listen(process.env.port || 3000, () => {
    logger.warn('Listening on port 3000')
});
